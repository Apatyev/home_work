/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
    /**
     * @returns {number}
     */
    function getRandomInt() {
        return Math.floor(Math.random() * 0x1000000);
    }

    /**
     * @returns {string}
     */
    function getRandomBackgroundColor() {
        return '#' + ("000000" + getRandomInt().toString(16)).substr(-6)
    }

    /**
     * returns void
     */
    function setRandomBackgroundColor() {
        var div = document.getElementById('myDiv');
        div.style.background = getRandomBackgroundColor();
    }

    var app = document.getElementById('app');
    var div = document.createElement('div');
    var button = document.createElement('button');
    button.classList.add('iButton');
    button.innerText = 'Test Button';
    button.style.width = '50px';
    button.style.height = '20px';
    button.style.position = 'absolute';
    button.style.bottom = '20px';
    button.style.left = '35%';
    button.addEventListener("click", setRandomBackgroundColor, false);

    div.id = 'myDiv';
    div.style.width = '500px';
    div.style.height = '500px';
    div.style.background = getRandomBackgroundColor();
    div.style.margin = 'auto';
    div.style.border = '3px solid #73AD23';
    div.style.padding = '10px';
    div.style.display = 'table';
    div.style.position = 'relative';
    div.appendChild(button);
    app.appendChild(div);





