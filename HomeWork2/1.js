
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  /**
   * returns void
   */
  function hideAllTabs() {
      var elements = document.querySelectorAll('div > div');
      elements.forEach(function (element) {
          element.classList.remove('active');
      })
  }

  /**
   * returns void
   */
  function activateTab(tabNumber) {

      var tabContainer = document.getElementById('tabContainer');
      var tabElements = tabContainer.getElementsByTagName('div');
      var tabElementsArr = Array.from(tabElements);
      for (var i = 0; i < tabElementsArr.length; i++) {
          tabElementsArr[i].classList.remove('active')
          if (tabElementsArr[i].getAttribute('data-tab') === tabNumber) {
              tabElementsArr[i].classList.add('active');
              setTimeout(hideAllTabs, 3000);
          }
      }
  }

  var showButton = document.getElementsByClassName('showButton');
  var showButtonArr = Array.from(showButton);

  for (var i = 0; i < showButtonArr.length; i++) {
      showButtonArr[i].onclick = function(event) {
          var atrValue = event.target.getAttribute('data-tab')
          activateTab(atrValue);
      }
  }
