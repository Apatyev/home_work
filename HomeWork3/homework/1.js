/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку
       и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас
        после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = [
      'images/cat1.jpg',
      'images/cat2.jpg',
      'images/cat3.jpg',
      'images/cat4.jpg',
      'images/cat5.jpg',
      'images/cat6.jpg',
      'images/cat7.jpg',
      'images/cat8.jpg'
  ];
  var currentPosition = 0;

  var imgElement = document.createElement('img');
  imgElement.id = 'sliderImg';
  imgElement.src = OurSliderImages[0];
  imgElement.classList.add('w3-animate-zoom');
  var slider = document.getElementById('slider');
  window.onload = () => {
    slider.classList.add('parent');
    slider.appendChild(imgElement);
  }

  var prevSliderButton = document.getElementById('PrevSlide');
  prevSliderButton.addEventListener('click', prevSlide);
  var nextSliderButton = document.getElementById('NextSlide');
  nextSliderButton.addEventListener('click', nextSlide);

  function prevSlide() {
      var sliderImg = document.getElementById('sliderImg');
      sliderImg.remove();
      var prevImg = document.createElement('img');
      if (currentPosition !== 0) {
        currentPosition--;
      } else {
        currentPosition = OurSliderImages.length - 1;
      }
      prevImg.id = 'sliderImg';
      prevImg.classList.add('w3-animate-left');
      prevImg.src = OurSliderImages[currentPosition];

      slider.appendChild(prevImg);
  }

  function nextSlide() {
      var sliderImg = document.getElementById('sliderImg');
      sliderImg.remove();
      var nextImg = document.createElement('img');
      if (currentPosition === OurSliderImages.length - 1) {
        currentPosition = 0;
      } else {
          currentPosition++;
      }
      nextImg.id = 'sliderImg';
      nextImg.classList.add('w3-animate-right');
      nextImg.src = OurSliderImages[currentPosition];
      slider.appendChild(nextImg);
  }
